// IMPORTING REACT CORE AND ROUTER
import React, { Component } from 'react';
import { NavLink } from "react-router-dom";

// IMPORTING NAVLINK PLUGIN'S CSS
import './Navlink.css';

class Navlink extends Component {

    /**
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            isLoaded: false
        };
    }

    /**
     * After construct
     */
    componentDidMount() {
        this.setState({
            isLoaded: true,
            link: this.props.targetLink + this.props.targetId
        });
    }

    /**
     * @returns {XML}
     */
    render() {
        const { error, isLoaded} = this.state;

        if (error) {
            return <div class="text-danger">Navlink error: {error.message}</div>; // @TODO make an errors plugin
        } else if (!isLoaded) {
            return <div class="text-primary">Loading ...</div>; // @TODO make a loading plugin
        } else {

            return (
                <NavLink exact to={this.state.link} class="btn btn-primary" title={this.props.title}>
                    {this.props.title}
                </NavLink>
            );
        }
    }
}

export default Navlink;
