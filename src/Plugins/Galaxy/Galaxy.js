// IMPORTING REACT CORE AND ROUTER
import React, { Component } from 'react';
import { Route, NavLink, HashRouter } from "react-router-dom";

// IMPORTING MAIN GALAXY LAYOUTS MODULES
import Home from "../../Layout/Home/Home";
import Products from "../../Layout/Products/Products";
import Product from "../../Layout/Product/Product";

// IMPORTING GALAXY'S CSS
import './Galaxy.css';

// RENDER THE GALXY
class Galaxy extends Component {

    /**
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            isLoaded: false
        };
    }

    /**
     * Galxy main render
     * @returns {XML}
     */
    render() {
        return (

            <HashRouter>
                <div>
                    <ul className="header">
                        <li><NavLink exact to="/">CHANEL</NavLink></li>
                        <li><NavLink to="/products">Produits printemps 2018</NavLink></li>
                        <li><h4><span id="app_title" class="chanel_breadcromb"></span></h4></li>
                    </ul>
                    <div className="content">
                        <Route exact path="/" component={Home}/>
                        <Route path="/products" component={Products}/>
                        <Route path="/product/:productId" component={Product}/>
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default Galaxy;
