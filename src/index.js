// IMPORTING REACT CORE
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

// IMPORTING THE CHANEL GALXY APP (CSS + JS)
import './index.css';
import Galaxy from './Plugins/Galaxy/Galaxy';

// RENDER THE APP INTO THE GALAXY MAIN LAYOUT
ReactDOM.render(<Galaxy />, document.getElementById('appContainer'));

// REGISTER REACT BOOTSTRAP SERVICES
registerServiceWorker();
