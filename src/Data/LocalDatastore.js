// @TODO: Connect to cloud APIs REST with axios and use redux to manage it !

var LocalDatastore = [
    {id: 1, name: "Gardenia", description:"he ultimate floral fragrance, intense and voluptuous, that blossoms with notes of Gardenia, a creamy heart and an imaginative trail. An absolutely feminine fragrance that captures the radiant light of summer.", stock_level: 12, image:"assets/product/gardenia.jpg"},
    {id: 2, name: "Gabrielle Chanel", description:"The new women's fragrance by CHANEL is a luminous composition. Inspired by Gabrielle Chanel, the House of CHANEL perfumer Olivier Polge, working with the CHANEL Laboratory of Creation and Development of Perfumes, composed a solar flower based on a bouquet of 4 white flowers.", stock_level: 30, image:"assets/product/gabrielle.jpg"},
    {id: 3, name: "Chance eau vive", description:"It comes and goes, it never stays still... and you only have a few seconds to seize your chance. It is unpredictable and appears when you least expect it, but if you decide to seize it, anything is possible. \"A chance came up, I seized it.\" Mademoiselle Chanel knew that her real chance was the one of her own creation, a state of mind, a way of being.", stock_level: 50, image:"assets/product/chance.jpg"}
];

export default LocalDatastore;