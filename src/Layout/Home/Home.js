// IMPORTING REACT CORE
import React, { Component } from 'react';

// IMPORTING HOME'S CSS
import './Home.css';

class Home extends Component {

    /**
     * @param props
     */
    constructor(props) {
        super(props);

        this.title = 'Accueil';

        this.state = {
            error: null,
            isLoaded: false
        };
    }

    /**
     * After construct
     */
    componentDidMount() {
        this.setState({
            isLoaded: true
        });

        document.getElementById('app_title').innerHTML = this.title;
    }

    /**
     * @returns {XML}
     */
    render() {
        return (
            <h1>HOME CONTENT</h1>
        );
    }
}

export default Home;
