// IMPORTING REACT CORE
import React, { Component } from 'react';
import { HashRouter } from "react-router-dom";

// IMPORTING NETWORK XHTTP REQUESTER
// import axios from 'axios'; // => For XHTTP, but not used for this demo, bypassed by a local datastore here

// IMPORTING CHANEL SERVICES PLUGINS
import isAuth from '../../Services/Middleware'

// IMPORTING CHANEL STRUCTURE PLUGINS
import Navlink from '../../Plugins/Commons/Actioners/Navlink/Navlink';

// IMPORTING DATA...
import LocalDatastore from '../../Data/LocalDatastore'; // => @TODO Replace by redux...
import Translation from '../../Data/Translations'
import Constants from '../../Data/Constants'

// IMPORTING PRODUCT PAGE'S CSS
import './Product.css';

class Product extends Component {

    /**
     * @param props
     */
    constructor(props) {
        super(props);

        isAuth();

        this.state = {
            error: null,
            isLoaded: false,
            item: {
                id: -1,
                name: '',
                description: '',
                stock_level: 0,
                price: 0.00,
                image: "assets/product/default.jpg"
            }
        };
    }

    /**
     * After construct
     */
    componentDidMount() {

        var productId = this.props.match.params.productId,
            item = null;

        if (productId <= 0) {
            return this.setState({
                isLoaded: true,
                error: {
                    message: 'Le produit sélectionné est introuvable'
                }
            });
        }

        for (var i= 0; i < LocalDatastore.length; i++) {
            if (LocalDatastore[i].id == productId) {
                item = LocalDatastore[i];
                break;
            }
        }

        if (!item) {
            return this.setState({
                isLoaded: true,
                error: {
                    message: 'Le produit sélectionné est introuvable'
                }
            });
        }

        this.setState({
            isLoaded: true,
            item: item,
            productId: productId
        });

        document.getElementById('app_title').innerHTML = "CHANEL > Produits > " + item.name;
    }

    /**
     * @returns {XML}
     */
    render() {
        const { error, isLoaded, item} = this.state;
        if (error) {
            return <div class="text-danger"><b>/!\ {error.message} /!\</b></div>;  // @TODO make an errors plugin
        } else if (!isLoaded) {
            return <div class="text-primary">Chargement du produit ...</div>; // @TODO make a loading plugin
        } else {

            return (
                <div class="row">
                    <div class="col-4">
                        <figure>
                            <img class="card-img-top" src={item.image} alt={item.name}/>
                            <figcaption>{item.name}</figcaption>
                        </figure>
                    </div>
                    <div class="col-8">
                        <p>{item.description}</p>
                        <p>Stock level: {item.stock_level}</p>
                        <HashRouter>
                            <Navlink exact targetLink="/product/" targetId={item.id} class="btn btn-primary" title={Translation[Constants.local]['buttonBuy']}></Navlink>
                        </HashRouter>
                    </div>
                </div>
            );
        }
    }
}

export default Product;
