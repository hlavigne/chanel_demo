// IMPORTING REACT CORE AND ROUTER
import React, { Component } from 'react';
import { HashRouter } from "react-router-dom";

// IMPORTING NETWORK XHTTP REQUESTER
// import axios from 'axios'; // => For XHTTP, but not used for this demo, bypassed by a local datastore here

// IMPORTING CHANEL SERVICES PLUGINS
import isAuth from '../../Services/Middleware'

// IMPORTING CHANEL STRUCTURE PLUGINS
import Navlink from '../../Plugins/Commons/Actioners/Navlink/Navlink';

// IMPORTING DATA...
import LocalDatastore from '../../Data/LocalDatastore'; // => @TODO Replace by redux...
import Translation from '../../Data/Translations'
import Constants from '../../Data/Constants'


// IMPORTING PRODUCT CSS
import './Products.css';

class Products extends Component {

    /**
     * @param props
     */
    constructor(props) {
        super(props);

        isAuth();

        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    /**
     * After construct
     */
    componentDidMount() {
        this.setState({
            isLoaded: true,
            items: LocalDatastore
        });

        document.getElementById('app_title').innerHTML = "CHANEL > Produits";
    }

    /**
     * @returns {XML}
     */
    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div class="text-danger">Erreur: {error.message}</div>; // @TODO make an errors plugin
        } else if (!isLoaded) {
            return <div class="text-primary">Chargement des produits ...</div>; // @TODO make a loading plugin
        } else {
            return (
                <HashRouter>
                    <ul class="listing">
                    {items.map(item => (
                        <li>
                            <div class="card">
                                <figure>
                                    <img class="card-img-top" src={item.image} alt={item.name} />
                                    <figcaption>{item.name}</figcaption>
                                </figure>
                                <div class="card-body">
                                <span>
                                    <h1 class="card-title">{item.name}</h1>
                                </span>
                                    <h6 class="card-text">{item.description}</h6>
                                    <Navlink exact targetLink="/product/" targetId={item.id} class="btn btn-primary" title={Translation[Constants.local]['buttonViewMore']}></Navlink>
                                </div>
                            </div>
                        </li>
                    ))}
                    </ul>
                </HashRouter>
            );
        }
    }
}

export default Products;
