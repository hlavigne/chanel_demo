# CHANEL ARCHITECHTURE DEMO
by Hugo Lavigne


# Technical information

## Stack 
- NodeJS 8.9.4
- ES6
- React
- [Bootstrap](http://getbootstrap.com/) 
- Git

## Install

The first step is to install all dependencies:

    > cd /path/to/root/folder
    > npm install

## Start project

To build the project in dev mode with the hot reload after any changes:

    > npm start

After that, you can reach the project at [localhost:3000](http://localhost:3000) 

## Build in production and start
To build in production mode the project:

    > npm build
    > npm start
    
The whole app will be transpiled to es5 in the folder `build/`
